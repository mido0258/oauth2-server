<?php


namespace App\Service;

class FileSessionStorage implements StorageInterface
{

    private string $path;

    public function __construct(string $path)
    {
        if (!is_dir($path) && !mkdir($path, 0777, true) && !is_dir($path)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $path));
        }

        $this->path = $path;
    }

    public function set(string $key, $data, int $ttl): void
    {
        $file = fopen($this->path . "/" . $key, 'wb+');
        while (false === flock($file, LOCK_EX)) {
            usleep(500);
        }
        fwrite($file, $data);
        fflush($file);
        flock($file, LOCK_UN);

        fclose($file);
    }

    public function delete(string $key): void
    {
        if (!is_file($this->path . "/" . $key)) {
            return;
        }
        unlink($this->path . "/" . $key);
    }

    public function garbageCollect(): void
    {
    }

    public function get(string $key, ?callable $expired = null)
    {
        if (!is_file($this->path . "/" . $key)) {
            return "[]";
        }
        file_get_contents($this->path . "/" . $key);
    }
}
