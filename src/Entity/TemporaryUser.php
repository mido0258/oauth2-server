<?php


namespace App\Entity;

use Symfony\Component\Security\Core\User\UserInterface;

class TemporaryUser implements UserInterface
{

    private User $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getRoles(): array
    {
        return ["ROLE_AUTH"];
    }

    public function getPassword()
    {
        return $this->user->getPassword();
    }

    public function getSalt()
    {
        return $this->user->getSalt();
    }

    public function getUsername()
    {
        return $this->user->getUsername();
    }

    public function eraseCredentials()
    {
        return $this->user->eraseCredentials();
    }
}
