<?php

namespace App\Entity;

use ParagonIE\HiddenString\HiddenString;
use Ramsey\Uuid\UuidInterface;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Webauthn\PublicKeyCredentialUserEntity;

/**
 * @ORM\Entity()
 * @ORM\Table(name="user_entity")
 */
class User implements UserInterface
{

    /**
     * @ORM\Id()
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     */
    private ?UuidInterface $uuid = null;
    /**
     * @ORM\Column(type="string")
     */
    private string $name;

    /**
     * @ORM\Column(type="string")
     */
    private string $surname;

    /**
     * @ORM\Column(type="string")
     */
    private string $email;

    /**
     * @ORM\Column(type="string")
     */
    private string $password;

    /**
     * @ORM\Column(type="boolean", options={"default": "false"})
     */
    private bool $fido = false;

    /**
     * User constructor.
     * @param string $name
     * @param string $surname
     * @param string $email
     * @param HiddenString $password
     */
    public function __construct(string $name, string $surname, string $email, HiddenString $password)
    {
        $this->name = $name;
        $this->surname = $surname;
        $this->email = $email;
        $this->password = $password->getString();
    }

    /**
     * @return string|null
     */
    public function getUuid(): ?string
    {
        return $this->uuid instanceof UuidInterface ? $this->uuid->toString() : null;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->name ." ".$this->surname;
    }

    /**
     * @return string
     */
    public function getSurname(): string
    {
        return $this->surname;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    public function getRoles()
    {
        return ["ROLE_USER"];
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getSalt()
    {
        return "";
    }

    public function getUsername()
    {
        return $this->getEmail();
    }

    public function eraseCredentials()
    {
        return null;
    }

    public function isFido(): bool
    {
        return $this->fido;
    }

    public function setFido(bool $fido): void
    {
        $this->fido = $fido;
    }
}
