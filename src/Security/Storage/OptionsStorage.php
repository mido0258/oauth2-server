<?php


namespace App\Security\Storage;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Webauthn\Bundle\Security\Storage\StoredData;

class OptionsStorage implements \Webauthn\Bundle\Security\Storage\OptionsStorage
{

    public function store(Request $request, StoredData $data, ?Response $response = null): void
    {
        $session = $request->getSession()->getName();
        file_put_contents("/tmp/$session", $data->serialize());
    }

    public function get(Request $request): StoredData
    {
        $session = $request->getSession()->getName();
        if (!is_file("/tmp/$session")) {
            throw new BadRequestHttpException('No public key credential fields options available for this session.');
        }
        return unserialize(file_get_contents("/tmp/$session"));
    }
}
