<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OpenIDDiscovery extends AbstractController
{
    /**
     * @Route("/keys", name="jwks_uri")
     */
    public function keys(): Response
    {
        $props = [
            "keys" => [
                [
                    "kty" => "RSA",
                    "n" => "y1jX3z8VLNPJIVMisuUuImfOT9hlmsaAm9Sa7SyVf4pIE2H5t4-V058KFtO2XsIaYEgfprmg9NWaxEvRfImvJE5jytzkze1BPkvLvpaFuQHfzu7EX5jki2DV1OLE-tfNxWbb8xbLR4lvkJMfAQi27pU7mfrAuRIVS8FY1fgKKYHp43LQQaXmNuDESK-68f1JvqMPpBmCCLk4D-u54EIVRyMISpOSdX5PpVvKVFvvIrhHuzORhbxn1FHevxZnXow771glaD63jCTwb-Ikw5iu_sQ9HwY0nLpkGxsKHlAwEAkjTsx50OfzVq5j1b6TQgOhri6j8c9vVyXvycOe-3bKbQ",
                    "e" => "AQAB",
                ],
            ]
        ];
        $response = new JsonResponse($props);
        $response->setEncodingOptions(JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        return $response;
    }

    /**
     * @Route("/.well-known/openid-configuration", name="oidc_discovery")
     */
    public function oidc(): Response
    {
        $props = [
            "issuer" => "https://id.alavio.eu",
            "authorization_endpoint" => "https://id.alavio.eu/authorize",
            "token_endpoint" => "https://id.alavio.eu/token",
            "userinfo_endpoint" => "https://id.alavio.eu/user",
            "revocation_endpoint" => "https://id.alavio.eu/revoke",
            "introspection_endpoint" => "https://id.alavio.eu/introspect",
            "jwks_uri" => "https://id.alavio.eu/keys",
            "id_token_signing_alg_values_supported" => [
                "RS256"
            ],
            "response_types_supported" => [
                "code",
                "none"
            ],
            "subject_types_supported" => [
                "public"
            ],
            "scopes_supported" => [
                "openid",
                "email",
                "profile"
            ],
            "token_endpoint_auth_methods_supported" => [
                "client_secret_post",
                "client_secret_basic"
            ],
            "code_challenge_methods_supported" => [
                "plain",
                "S256"
            ],
            "grant_types_supported" => [
                "authorization_code",
                "refresh_token"
            ]
        ];
        $response = new JsonResponse($props);
        $response->setEncodingOptions(JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        return $response;
    }
}
