<?php

namespace App\Controller;

use App\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class UserController extends AbstractController
{
    /**
     * @Route("/", name="app_base")
     * @return Response
     */
    public function show(): Response
    {
        if ($this->getUser() instanceof User) {
            return $this->redirectToRoute("app_login_finish");
        }
        return $this->redirectToRoute("app_login");
    }

    /**
     * @Route("/status", name="app_status")
     * @return Response
     */
    public function status(): Response
    {
        return new Response("OK", 200);
    }

    /**
     * @Route("/user", name="app_user")
     * @param UserInterface $user
     * @return Response
     */
    public function user(UserInterface $user): Response
    {
        $response = [
            "sub" => $user->getUuid(),
            "name" => $user->getFullName(),
            "given_name" => $user->getName(),
            "family_name" => $user->getSurname(),
            "preferred_username" => $user->getUsername(),
            "email" => $user->getEmail()
        ];
        return new JsonResponse($response, 200);
    }
}
