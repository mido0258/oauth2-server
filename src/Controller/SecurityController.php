<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Guard\Token\PostAuthenticationGuardToken;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Http\Util\TargetPathTrait;

class SecurityController extends AbstractController
{
    use TargetPathTrait;

    private const PROVIDER = "oauth";

    /**
     * @Route("/login", name="app_login")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error
        ]);
    }

    /**
     * @Route("/fido", name="app_login_fido")
     * @return Response
     */
    public function fido(): Response
    {
        return $this->render('security/fido.html.twig', ["username" => $this->getUser()->getUsername()]);
    }

    /**
     * @Route("/finish", name="app_login_finish")
     * @param Request $request
     * @return Response
     */
    public function finish(Request $request, TokenStorageInterface $tokenStorage, SessionInterface $session): Response
    {
        $token = new PostAuthenticationGuardToken(
            $tokenStorage->getToken()->getUser(),
            self::PROVIDER,
            $tokenStorage->getToken()->getUser()->getRoles()
        );

        $session->set("_security_".self::PROVIDER, serialize($token));

        if ($targetPath = $this->getTargetPath($request->getSession(), self::PROVIDER)) {
            $link = $targetPath;
        } else {
            $link = "https://alavio.eu";
        }
        return $this->render('security/finish.html.twig', ["link" => $link]);
    }
}
