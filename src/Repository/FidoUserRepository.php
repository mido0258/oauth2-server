<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\FidoUser;
use Doctrine\Persistence\ManagerRegistry;
use Ramsey\Uuid\Uuid;
use Webauthn\Bundle\Repository\AbstractPublicKeyCredentialUserEntityRepository;
use Webauthn\PublicKeyCredentialUserEntity;

final class FidoUserRepository extends AbstractPublicKeyCredentialUserEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FidoUser::class);
    }

    public function createUserEntity(string $username, string $displayName, ?string $icon): PublicKeyCredentialUserEntity
    {
        $id = Uuid::uuid4()->toString();

        return new FidoUser($id, $username, $displayName, $icon, []);
    }

    public function saveUserEntity(PublicKeyCredentialUserEntity $userEntity): void
    {
        if (!$userEntity instanceof FidoUser) {
            $userEntity =  new FidoUser(
                $userEntity->getId(),
                $userEntity->getName(),
                $userEntity->getDisplayName(),
                $userEntity->getId()
            );
        }

        parent::saveUserEntity($userEntity);
    }

    public function find(string $username): ?FidoUser
    {
        $qb = $this->getEntityManager()->createQueryBuilder();

        $res =  $qb->select('u')
        ->from(FidoUser::class, 'u')
        ->where('u.name = :name')
        ->setParameter(':name', $username)
        ->setMaxResults(1)
        ->getQuery()
        ->getOneOrNullResult()        ;
        return $res;
    }
}
