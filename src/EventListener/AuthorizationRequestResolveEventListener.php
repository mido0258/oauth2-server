<?php

namespace App\EventListener;

use Trikoder\Bundle\OAuth2Bundle\Event\AuthorizationRequestResolveEvent;

class AuthorizationRequestResolveEventListener
{

    public function onTrikoderOauth2AuthorizationRequestResolve(AuthorizationRequestResolveEvent $event): void
    {
        $event->resolveAuthorization(AuthorizationRequestResolveEvent::AUTHORIZATION_APPROVED);
    }
}
