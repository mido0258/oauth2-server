FROM php:8.0.9-cli-alpine as php

RUN apk add postgresql-dev automake gcc autoconf make m4  g++
RUN pecl install apcu swoole && docker-php-ext-install pgsql pdo_pgsql

FROM alpine:3.14.1

COPY --from=php /usr/local/lib/php/extensions /usr/local/lib/php/extensions
COPY --from=php /usr/local/etc /usr/local/etc
COPY --from=php /usr/local/bin/php /usr/local/bin/php

RUN apk add --no-cache libpq argon2-libs sqlite-libs libxml2 libcurl oniguruma libedit 	libstdc++ libsodium && mkdir -p /app/var && chmod -R 777 /app/var

COPY .docker/php/php-prod.ini /usr/local/etc/php/php.ini

COPY .  "/app"

WORKDIR /app
EXPOSE 8080
CMD bin/console swoole:server:run --port 8080


