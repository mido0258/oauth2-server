#!/bin/sh
while ! nc -z pgsql 5432 &> /dev/null; do sleep 1; done;

composer install
bin/console cache:clear
cd /app/ && bin/console doctrine:migrations:migrate --allow-no-migration  -n
bin/console swoole:server:run --port 8080
