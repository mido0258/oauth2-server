#!/bin/sh
if test -f "/app/.clean"; then
    rm -R /app/node_modules /app/www/node-modules /app/yarn.lock /app/package-lock.json /app/www/yarn.lock /app/www/package-lock.json
fi
LIBRARY_PATH=/lib:/usr/lib && cd /app && yarn install && yarn start