import { useRegistration } from '@web-auth/webauthn-helper';

// We want to register new authenticators
const register = useRegistration({
    actionUrl: '/webauthn/auth/register',
    optionsUrl: '/webauthn/auth/registeropts'
});

register({
    username: 'mido@alavio.eu',
    displayName: 'mido@alavio.eu'
})
    .then(function (resp) {
        console.log('Registration success');
    })
    .catch((error) => console.log('Registration failure'));

