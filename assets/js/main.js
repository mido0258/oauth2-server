import { useLogin } from '@web-auth/webauthn-helper';

if (document.getElementsByClassName('info').length > 0) {
    const login = useLogin({
        actionUrl: '/webauthn/auth/login',
        optionsUrl: '/webauthn/auth/options'
    });

    login({
        username: document.getElementsByClassName('info')[0].getAttribute('data-username')
    })
        .then((response) => {
            location.href = '/finish';
        })
        .catch((error) => {
            console.log(error);
            location.href = '/login';
        });
}

let redirect = document.getElementsByClassName('redirect');
if (redirect.length > 0) {
    location.href = redirect[0].getAttribute('data-redirect');
}
