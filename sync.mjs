import BrowserSync from 'browser-sync'
import ParcelCore from "@parcel/core";

const {default: Parcel} = ParcelCore;

BrowserSync.create("default");

let browserSync = BrowserSync.init({
    port: 3000,
    open: false,
    proxy: "app:8080",
    middleware: [],
    plugins: ["browser-sync-console"],
    logLevel: 'info',
    logPrefix: 'BS',
    logConnections: true,
    logFileChanges: true,
    ghostMode: {
        clicks: true,
        forms: true,
        scroll: false,
        location: true
    }
}, async () => {
    let cback = (e, v, t) => {
        if (v.type === "buildSuccess") {
            browserSync.reload();
        }
    }

    let bundler = new Parcel({
        entries: ["assets/js/main.js", "assets/js/auth.js", "assets/styles/main.css"],
        defaultTargetOptions: {
            shouldOptimize: true,
            sourceMaps: true,
            publicUrl: "/",
            mode: "production",
            distDir: "public/build",
            isLibrary: false,
        },
        watch: true,
        onBuildSuccess: cback
    });

    await bundler.watch(cback);
});

